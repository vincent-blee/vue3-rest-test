class DateFormat {
  getRedeableDate(date) {
    if (date === undefined) {
      date = new Date();
    }
    if (false === date instanceof Date) {
      date = new Date(date.slice(0, 16).split("T")[0]);
    }
    if (date instanceof Date) {
      return Intl.DateTimeFormat().format(date);
    }
  }
  getInputDate(date) {
    if (date instanceof Date) {
      date = date.toISOString();
    }
    if (date !== "" && date.includes("T")) {
      return date.split("T")[0];
    }
    return date;
  }
  prepareDate(date) {
    if (date instanceof Date) {
      date = date.toJSON();
    }
    return new Date(date);
  }
}
export default new DateFormat();
