import http from "../http-common";
import DateFormat from "../utils/DateFormat";

class PlayerDataService {
  getAll() {
    return http.get("/players");
  }
  get(id) {
    return http.get(`/players/${id}`);
  }
  create(player) {
    player.Birthday = DateFormat.prepareDate(player.Birthday);
    return http.post("/players", player);
  }
  update(player) {
    player.Birthday = new Date(player.Birthday);
    return http.put(`/players/${player.ID}`, player);
  }
  remove(player) {
    return http.delete(`/players/${player.ID}`, player);
  }
}

export default new PlayerDataService();
